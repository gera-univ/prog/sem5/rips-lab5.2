package org.spiralarms.RIPSLab5_1;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            StackTraceElement[] stack = Thread.currentThread().getStackTrace();
            StackTraceElement main = stack[stack.length - 1];
            String mainClass = main.getClassName();
            System.out.println("usage: java " + mainClass + " ip port");
            return;
        }

        String ip = args[0];
        int port = Integer.parseInt(args[1]);
        try (Socket s = new Socket(ip, port);
             Scanner in = new Scanner(s.getInputStream(), StandardCharsets.UTF_8)) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                System.out.println(line);
            }
        }
    }
}
