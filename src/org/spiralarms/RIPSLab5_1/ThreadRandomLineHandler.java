package org.spiralarms.RIPSLab5_1;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ThreadRandomLineHandler implements Runnable {
    private final Socket incoming;
    private final RandomLineGenerator rlGen;

    public ThreadRandomLineHandler(Socket incoming, RandomLineGenerator rlGen) {
        this.incoming = incoming;
        this.rlGen = rlGen;
    }

    @Override
    public void run() {
        try (OutputStream outputStream = incoming.getOutputStream()) {
            PrintWriter out = new PrintWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8), true);
            out.println(rlGen.getRandomLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
