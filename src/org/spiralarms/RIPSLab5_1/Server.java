package org.spiralarms.RIPSLab5_1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    int port;
    RandomLineGenerator rlGen;

    public Server(String filename, int port) throws IOException {
        rlGen = new RandomLineGenerator(filename);
        this.port = port;
    }

    public void start() {
        try (ServerSocket s = new ServerSocket(port)) {
            while (true) {
                Socket incoming = s.accept();
                Runnable r = new ThreadRandomLineHandler(incoming, rlGen);
                Thread t = new Thread(r);
                t.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            StackTraceElement[] stack = Thread.currentThread().getStackTrace ();
            StackTraceElement main = stack[stack.length - 1];
            String mainClass = main.getClassName ();
            System.out.println("usage: java " + mainClass + " filename port");
            return;
        }
        String filename = args[0];
        int port = Integer.parseInt(args[1]);

        Server s = new Server(filename, port);
        s.start();
    }
}
