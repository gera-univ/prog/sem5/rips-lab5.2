package org.spiralarms.RIPSLab5_1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class RandomLineGenerator {
    private ArrayList<String> lines;
    private Random random;

    public RandomLineGenerator(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        lines = new ArrayList<>();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }
        bufferedReader.close();
        this.random = new Random();
    }

    public String getRandomLine() {
        return lines.get(random.nextInt(lines.size()));
    }
}
